﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private dotnetdbContext _context;

        public UserController(dotnetdbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return Ok(_context.User.ToArray());
        }
    }
}